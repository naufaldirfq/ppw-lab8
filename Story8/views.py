from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.conf import settings	
from django.core import serializers
import requests
import json


response = {}

# Create your views here.
def profile(request):
	return render(request, 'profil.html')