$(function() {
    var Accordion = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    var links = this.el.find('.link');
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
  }

  Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
      $this = $(this),
      $next = $this.next();

    $next.slideToggle();
    $this.parent().toggleClass('open');

    if (!e.data.multiple) {
      $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
  } 

  var accordion = new Accordion($('#accordion'), false);
});


function nightmode() {
  var css1 = document.getElementById("background")
  var font = document.getElementById("font")
  var fontnight = document.getElementById("nightmode")

  if (css1.style.backgroundColor === 'white') {
        css1.style.backgroundColor = '#000A1E';
        font.style.color = 'white';
        fontnight.style.color = 'white';
  } else {
        css1.style.backgroundColor = 'white';
        font.style.color = 'black';
        fontnight.style.color = 'black';
  }
}

$("img").tooltip();

$(document).ready(function(){
  setTimeout(function() {
    console.log("hello")
    $('body')
      .removeClass('loading')
      .addClass('loaded');
  }, 3000);
});
