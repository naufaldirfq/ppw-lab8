from django import forms
import datetime
class Form_Subscribe(forms.Form):

    error_messages = {
        'required': 'Please type',
    }

    nama_attrs = {
        'type': 'text',
        'placeholder' : 'Nama Lengkap',
        'class' : 'form-control',
    }

    email_attrs = {
        'type': 'email',
        'id' : 'email',
        'placeholder' : 'Email',
        'class' : 'form-control',
    }

    password_attrs = {
        'type': 'password',
        'placeholder': 'Password',
        'class' : 'form-control'
    }

    nama = forms.CharField(label='Nama Lengkap', required=True, widget=forms.TextInput(attrs = nama_attrs))
    email = forms.EmailField(label='Alamat Email', required=True, widget=forms.TextInput(attrs = email_attrs))
    password = forms.CharField(required=True, widget = forms.PasswordInput(attrs = password_attrs))
