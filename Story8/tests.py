from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profile

# Create your tests here.
class Lab8UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/lab8/')
		self.assertEqual(response.status_code, 200)

	def test_using_profile_func(self):
		found = resolve('/lab8/')
		self.assertEqual(found.func, profile)

	def test_using_profile_page_template(self):
		response = Client().get('/lab8/')
		self.assertTemplateUsed(response, 'profil.html')