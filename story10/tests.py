from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import subscribe

# Create your tests here.
class Lab10UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/lab10/')
		self.assertEqual(response.status_code, 200)

	def test_using_subscribe_func(self):
		found = resolve('/lab10/')
		self.assertEqual(found.func, subscribe)

	def test_using_subscribe_page_template(self):
		response = Client().get('/lab10/')
		self.assertTemplateUsed(response, 'formsubscribe.html')