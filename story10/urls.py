from django.urls import path
from .views import *

urlpatterns = [
	path('', subscribe, name="form"),
	path('addSubscribe/', addSub, name="addSubscribe"),
	path('validasi', validasi, name="validasi"),
	path('unsubscribe/', unsubscribe, name='unsubscribe'),
	path('data', data, name = 'data'),
	path('delObject', delObject, name = 'delObject')

]
