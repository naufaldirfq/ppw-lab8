from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.conf import settings	
from django.core import serializers
import requests
import json
from .models import *
from .forms import *

# Create your views here.
def subscribe(request):
	form = Form_Subscribe(request.POST or None)
	return render(request, 'formsubscribe.html', {'form':form})

def unsubscribe(request):
	form = Form_Unsubscribe(request.POST or None)
	return render(request, 'formunsubscribe.html', {'form':form})

def data(request):
	getJson = requests.get('http://naufaldi-lab8.herokuapp.com/lab10/addSubscribe/').json()
	return JsonResponse(getJson)

@csrf_exempt
def addSub(request):
	if (request.method == 'POST'):
		enctype = "multipart/form-data"
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']

		subscriber = Model_Subscribe(nama = nama, email = email, password = password)
		subscriber.save()
		
		data = getObject(subscriber)
		return HttpResponse(data)
	else:
		sub = Model_Subscribe.objects.all().values()
		data = list(sub)
		return JsonResponse({'items':data}, safe = False)

@csrf_exempt
def validasi(request):
	enctype = "multipart/form-data"
	email = request.POST.get('email')
	data = {
		'is_taken':Model_Subscribe.objects.filter(email=email).exists()
		}
	return JsonResponse(data)

def getObject(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data

@csrf_exempt
def delObject(request):
	email = request.POST.get('email')
	nama = Model_Subscribe.objects.filter(email = email)
	data = list(nama)
	Model_Subscribe.objects.filter(email = email).delete()
	return JsonResponse({'items':data}, safe = False)
