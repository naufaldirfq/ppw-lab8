from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import table, data, table1, data_science

# Create your tests here.
class Lab9UnitTest(TestCase):
	def test_url_lab9_is_exist(self):
		response = Client().get('/lab9/')
		self.assertEqual(response.status_code, 200)

	def test_using_table_func(self):
		found = resolve('/lab9/')
		self.assertEqual(found.func, table)

	def test_using_books_page_template(self):
		response = Client().get('/lab9/')
		self.assertTemplateUsed(response, 'books.html')

	def test_url_lab9_data_is_exist(self):
		response = Client().get('/lab9/data')
		self.assertEqual(response.status_code, 200)

	def test_using_data_func(self):
		found = resolve('/lab9/data')
		self.assertEqual(found.func, data)