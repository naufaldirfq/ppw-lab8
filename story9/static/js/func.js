var counter =0;
$(document).ready(function() {
    $.ajax({
        type : 'GET',
        url : 'data',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
                for(var i=1; i<=data.items.length; i++) {
                    print+= "<td>" + "<img src='" + data.items[i-1].volumeInfo.imageLinks.thumbnail +"'></img>"+"</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.title + "</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.authors + "</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.publisher + "</td>";
                    print+= "<td style="+"'text-align:center'"+">";
                    print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';
                }
                $('tbody').append(print);
        }
    });
    $(document).on('click', '#button', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
        }
        $('#counter').html(counter);

    });
});

$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    sendToken(id_token);
    console.log("Logged In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("signed in");
            if (result.status === "0") {
                html = "<h4>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}
