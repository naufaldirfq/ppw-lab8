from django.urls import path
from .views import table, data, data_science, table1, index
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import *
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .views import home
from django.conf import settings

urlpatterns = [
	path('', table, name = 'table'),
	path('data', data, name = 'data'),
	path('data_science', data_science, name = 'data_science'),
	path('quilting', table1, name = 'table1'),
]
