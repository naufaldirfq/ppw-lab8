from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings
import requests
import json

# Create your views here.
def table(request):
	return render(request, 'books.html')

def data(request):
	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=programming').json()
	return JsonResponse(getJson)

def data_science(request):
	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	return JsonResponse(getJson)

def table1(request):
	return render(request, 'books1.html')

def index(request):
	return render(request, 'index.html')

def home(request):
	return render(request, 'home.html')
