var counter =0;
$(document).ready(function() {
    $.ajax({
        type : 'GET',
        url : 'data',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
                for(var i=1; i<=data.items.length; i++) {
                    print+= "<td>" + "<img src='" + data.items[i-1].volumeInfo.imageLinks.thumbnail +"'></img>"+"</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.title + "</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.authors + "</td>";
                    print+= "<td>"+data.items[i-1].volumeInfo.publisher + "</td>";
                    print+= "<td style="+"'text-align:center'"+">";
                    print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';
                }
                $('tbody').append(print);
        }
    });
    $(document).on('click', '#button', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
        }
        $('#counter').html(counter);

    });
});
