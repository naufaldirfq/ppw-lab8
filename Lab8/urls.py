"""Lab8 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import Story8.urls as Story8
import story9.urls as story9
import story10.urls as story10
import story11.urls as story11
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lab8/', include(('Story8.urls', 'Story8'), namespace = "story6")),
    path('lab9/', include(('story9.urls', 'story9'), namespace = "story9")),
    path('lab10/', include(('story10.urls', 'story10'), namespace = "story10")),
    path('lab11/', include(('story11.urls', 'story11'), namespace = "story11")),
    path('auth/', include('social_django.urls', namespace='social')),
    
]

